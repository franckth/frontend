describe('Trajet', () => {
  it('passes', () => {
    cy.visit('http://0.0.0.0:8000/');
    cy.get('#departure').type('Paris');
    cy.get('#arrival').type('Bruxelles');
    cy.get('#search').click();
    cy.get('#6687b30119ea6e6a6d05684f').click();
    cy.visit('http://0.0.0.0:8000/cart.html');
    cy.contains('Bruxelles');
    cy.get('#purchase').click();
    cy.visit('http://0.0.0.0:8000/bookings.html');
    cy.contains('Bruxelles');
  })
})